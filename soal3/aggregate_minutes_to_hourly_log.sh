#!/bin/bash

mkdir -p ~/log

tdatelh=$(date --date='1 hour ago' +"%Y%m%d")
tdate=$(date +"%Y%m%d")

thourlh=$(date --date='1 hour ago' +"%H")
thour=$(date +"%H")

for ((i=0;i<=59;i=i+1)); do
	str="$i"
	if [ "$i" -lt 10 ]; then
                str="0$i"
        fi
#	echo $str
	FILE=~/log/metrics_$tdatelh$thourlh$str.log
	if test -f "$FILE"; then
		awk -F" " 'BEGIN{OFS=","} NR!=1 {print $1}' ~/log/metrics_$tdatelh$thourlh$str.log >> ~/log/temp_metrics_agg_$tdate$thour.log
	fi
done

echo "type,mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size" > ~/log/metrics_agg_$tdate$thour.log

awk -F"," '
BEGIN{OFS=",";min1=min2=min3=min4=min5=min6=min7=min8=min9=min11=99999} 
$1 < min1 {min1=$1;}
$2 < min2 {min2=$2;}
$3 < min3 {min3=$3;}
$4 < min4 {min4=$4;}
$5 < min5 {min5=$5;}
$6 < min6 {min6=$6;}
$7 < min7 {min7=$7;}
$8 < min8 {min8=$8;}
$9 < min9 {min9=$9;}
$11 < min11 {min11=$11;}
END{print "minimum",min1,min2,min3,min4,min5,min6,min7,min8,min9,$10,min11}' ~/log/temp_metrics_agg_$tdate$thour.log >> ~/log/metrics_agg_$tdate$thour.log

awk -F"," '
BEGIN{OFS=",";max1=max2=max3=max4=max5=max6=max7=max8=max9=max11=0}
$1 > max1 {max1=$1}
$2 > max2 {max2=$2}
$3 > max3 {max3=$3}
$4 > max4 {max4=$4}
$5 > max5 {max5=$5}
$6 > max6 {max6=$6}
$7 > max7 {max7=$7}
$8 > max8 {max8=$8}
$9 > max9 {max9=$9}
$11 > max11 {max11=$11}
END{print "maximum",max1,max2,max3,max4,max5,max6,max7,max8,max9,$10,max11}' ~/log/temp_metrics_agg_$tdate$thour.log >> ~/log/metrics_agg_$tdate$thour.log

awk -F"," 'BEGIN{OFS=",";sum1=sum2=sum3=sum4=sum5=sum6=sum7=sum8=sum9=sum11=0;} {
sum1+=$1
sum2+=$2
sum3+=$3
sum4+=$4
sum5+=$5
sum6+=$6
sum7+=$7
sum8+=$8
sum9+=$9
sum11+=$11 }
END{sum1/=NR;sum2/=NR;sum3/=NR;sum4/=NR;sum5/=NR;sum6/=NR;sum7/=NR;sum8/=NR;sum9/=NR;sum11/=NR;
sum1=int(sum1)
sum2=int(sum2)
sum3=int(sum3)
sum4=int(sum4)
sum5=int(sum5)
sum6=int(sum6)
sum7=int(sum7)
sum8=int(sum8)
sum9=int(sum9)
sum11=int(sum11)
print "average",sum1,sum2,sum3,sum4,sum5,sum6,sum7,sum8,sum9,$10,sum11 "M"}
' ~/log/temp_metrics_agg_$tdate$thour.log >> ~/log/metrics_agg_$tdate$thour.log

rm ~/log/temp_metrics_agg_$tdate$thour.log

chmod 400 ~/log/metrics_agg_$tdate$thour.log
