#!/bin/bash

mkdir -p ~/log

tdate=$(date +"%Y%m%d")
thour=$(date +"%H")
tminute=$(date +"%M")

echo -e "mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size" > ~/log/metrics_$tdate$thour$tminute.log
free -m | awk 'BEGIN{OFS=","} NR!=1 {for(i=2;i<=NF;i++) printf $i",";}' >> ~/log/metrics_$tdate$thour$tminute.log
du -sh ~/ | awk 'BEGIN{OFS=","} {printf $2","$1}' >> ~/log/metrics_$tdate$thour$tminute.log

chmod 400 ~/log/metrics_$tdate$thour$tminute.log
