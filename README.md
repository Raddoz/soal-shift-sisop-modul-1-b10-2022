# soal-shift-sisop-modul-1-B10-2022

# Kelompok B10 Sistem Operasi | Praktikum Modul 1
    Elthan Ramanda B           <-->  5025201092
    Bimantara Tito Wahyudi     <-->  5025201227
    Frederick Wijayadi Susilo  <-->  5025201111

## Source Soal Shift 1
Soal dapat diakses <a href="https://docs.google.com/document/d/19aRcJHaIySZsoWnui8HaJDklxCca_3sn0b8NucCcn6I/edit?usp=sharing" target="_blank">disini</a>
## Penyelesaian Soal Shift 1

### Soal 1
Pada soal 1, dijelaskan jika Han, dan teman temannya diberikan tugas untuk mencari foto akan tetapi, karena laptop teman temannya rusak Han dengan baik hati menawarkan laptopnya untuk digunakan. Disini Han membuat sebuah program dimana :
<details>
  <summary markdown="span">Tugas 1a</summary>

    Han membuat sistem register pada script register.sh dan setiap user yang berhasil didaftarkan disimpan di dalam file ./users/user.txt. Han juga membuat sistem login yang dibuat di script main.sh
</details>

- Source Code

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;![1a](./img/1a.1.png)

- Cara Pengerjaan

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Tujuan : Membuat directory **users** yang kemudian membuat file **user.txt** didalam directory users

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Function `Username` bertugas untuk membaca input username yang dimasukkan dari input `read -p "Username: " username`. Kemudian dilanjut dengan perintah `if [ ! -d "users" ]` yang apabila belum ada directory **users** maka akan dibuat dan membuat file **user.txt** pada directory tersebut. Dilanjutkan mengecheck Username dengan perintah `if grep -q -w $username ./users/user.txt` dimana jika user meng-input username yang sudah ada pada file **user.txt** maka akan keluar tulisan **Username sudah dipakai** dan pada file **log.txt** akan print `REGISTER: ERROR User already exists` dengan tambahan format **MM/DD/YY hh:mm:ss** pada depannya.

<details>
  <summary markdown="span">Tugas 1b</summary>

  Demi menjaga keamanan, input password pada login dan register harus tertutup/hidden dan password yang didaftarkan memiliki    kriteria sebagai berikut
  - Minimal 8 karakter
  - Memiliki minimal 1 huruf kapital dan 1 huruf kecil
  - Alphanumeric
  - Tidak boleh sama dengan username
</details>

- Source Code

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;![1b](./img/1.b.png)

- Cara Pengerjaan

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**Tujuan : Membuat input password dengan ketentuan yang sudah tersedia**

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Terdapat 4 kriteria password, yaitu minimal 8 karakter maka digunakan `if [ ${#password} -lt 8 ]` jika kurang dari itu akan print **Password minimal 8 karakter!**, kemudian password memiliki minimal 1 huruf kapital dan 1 huruf kecil maka digunakan `elif [[ $password = ${password,,} || $password = ${password^^} ]]` jika tidak sesuai akan print **Password memiliki minimal 1 huruf kapital dan 1 huruf kecil!**, Password Alphanumeric, disini kami menggunakan 2 perintah yaitu `elif [[ "$password" =~ [^a-zA-Z0-9] ]]` dan `echo $password | grep -q [0-9]`, `if test $? -ne 0` untuk memastikan jika passwordnya harus memiliki huruf besar, huruf kecil, serta angka yang terinput, jika tidak sesuai maka akan print **Password alphanumeric!**, dan terakhir password tidak boleh sama dengan username maka digunakan perintah `elif [[ "$password" = "$username" ]]` jika password sama maka akan print **Password tidak boleh sama dengan username!**

<details>
  <summary markdown="span">Tugas 1c</summary>

    Setiap percobaan login dan register akan tercatat pada log.txt dengan format : MM/DD/YY hh:mm:ss MESSAGE. Message pada log akan berbeda tergantung aksi yang dilakukan user.
    - Ketika mencoba register dengan username yang sudah terdaftar, maka message pada log adalah REGISTER: ERROR User already exists
    - Ketika percobaan register berhasil, maka message pada log adalah REGISTER: INFO User USERNAME registered successfully
    - Ketika user mencoba login namun passwordnya salah, maka message pada log adalah LOGIN: ERROR Failed login attempt on user USERNAME
    - Ketika user berhasil login, maka message pada log adalah LOGIN: INFO User USERNAME logged in
</details>

- Source Code

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;![1c](./img/1c1.png)
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;![1c](./img/1c2.png)
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;![1c](./img/1c3.png)
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;![1c](./img/1c4.png)

- Cara Pengerjaan

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**Tujuan : Print pada log.txt setiap percobaan login dan register**

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Pada soal diminta membuat 4 kriteria log yang tercatat pada **log.txt**, dimana yang pertama adalah register dengan username yang sudah terdaftar, menggunakan `if grep -q -w $username ./users/user.txt` untuk mencari username pada file **user.txt** dan apabila terdapat username yang sama dengan register input maka akan print **REGISTER: ERROR User already exists**, kedua ketika register berhasil, menggunakan `echo "$username $password" >> ./users/user.txt` yang kemudian akan print **REGISTER:INFO '$username' registered succesfully**, ketiga ketika user mencoba login dengan username yang terdaftar akan tetapi passwordnya salah, menggunakan `if grep -q -w "$username $password" ./users/user.txt` untuk menyamakan input dari password yang kemudian dicari pada file **user.txt** apabila username yang diinput sama akan tetapi password salah maka akan print **LOGIN: ERROR Failed login attempt on user '$username'**, keempat ketika user menginput username dan password yang ter-register dan benar semua, menggunakan `if grep -q -w "$username $password" ./users/user.txt` sama seperti ketiga jika input user sama dengan yang terdaftar pada file **user.txt** maka akan print **LOGIN: INFO User '$username' logged in**

<details>
  <summary markdown="span">Tugas 1d</summary>

    Setelah login, user dapat mengetikkan 2 command dengan dokumentasi sebagai berikut :
    - dl N ( N = Jumlah gambar yang akan didownload) Untuk mendownload gambar dari https://loremflickr.com/320/240 dengan jumlah sesuai dengan yang diinputkan oleh user. Hasil download akan dimasukkan ke dalam folder dengan format nama YYYY-MM-DD_USERNAME. Gambar-gambar yang didownload juga memiliki format nama PIC_XX, dengan nomor yang berurutan (contoh : PIC_01, PIC_02, dst. ).  Setelah berhasil didownload semua, folder akan otomatis di zip dengan format nama yang sama dengan folder dan dipassword sesuai dengan password user tersebut. Apabila sudah terdapat file zip dengan nama yang sama, maka file zip yang sudah ada di unzip terlebih dahulu, barulah mulai ditambahkan gambar yang baru, kemudian folder di zip kembali dengan password sesuai dengan user.
    - att Menghitung jumlah percobaan login baik yang berhasil maupun tidak dari user yang sedang login saat ini.
</details>

- Source Code

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;![1d](./img/1d.png)

- Cara Pengerjaan

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**Tujuan : Mendownload gambar dan membaca attempt login pada user yang sedang login**

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Terdapat 3 function yaitu `Command`, `doZip`, `dl_files`. Pertama Command, dimana pada function ini akan membaca input dari user. Apabila input membaca `dl n` maka akan membaca function doZip dimana pada fungsi ini pertama akan membuat folder dengan format `printf '%(%Y-%m-%d)T'_$username`, dilanjut dengan `if ! [ -f "$folder_name.zip" ]` yang jika masuk ke `then` maka akan membuat directory dengan nama yang sesuai format yaitu **YYYY-MM-DD_USERNAME**, namun jika masuk `else` maka fungsi akan meng-unzip file .zip tersebut yang dilanjutkan dengan menghitung jumlah file dalam directory tersebut dengan perintah `ls` yang sesudah mendapatkan daftar file yang ada pada directory tersebut dilanjutkan dengan perintah `wc` untuk menghitung jumlah baris dengan parameter `-l` yang kemudian dimasukkan pada variabel `i` agar bisa ditambah dengan `n` yang diinput oleh user. kemudian lanjut untuk mendownload file masuk pada function `dl_files` dimana pada directory tersebut akan dihitung dan menambah gambar bila sudah ada (tidak mereplace) dengan `for (( i=$count+1; i <= n; i++ ));` yang kemudian mendownload dari https://loremflickr.com/320/240 yang apabila sudah berhasil download maka folder tersebut akan di zip dengan tambahan input password dari user agar file.zip tersebut terenkripsi yang kemudian dilanjutkan dengan meremove folder yang sudah di zip. Selanjutnya apa bila input membaca `att` maka dengan perintah `awk -v temp=$username '/LOGIN/&&$0~" " temp " " {++n; print} END {print "Login Attempt: " n}' log.txt` akan membuka **log.txt** yang dilanjutkan dengan mencari **LOGIN** dan **$username** untuk dicari disetiap barisnya yang kemudian di print dan setelah perintah awk selesai di eksekusi akan muncul jumlah login attempt nya pada `n`

- Kendala

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Terdapat banyak kendala pada soal 1D seperti spasi yang berpengaruh, tanda `'` juga yang salah penempatan serta kurang pahamnya pada beberapa segmen sehingga masuk revisi

- Output

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;![out1](./img/OutputSoal1.1.png)

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;![out2](./img/OutputSoal1.2.png)

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;![out2](./img/OutputSoal1.3.png)

### Soal 2
Pada soal 2, dipaparkan bahwa website "https://daffa.info" di hack oleh seseorang yang tidak bertanggung jawab dan pada log website, ditemukan banyak request berbahaya. Dapos meminta bantuan untuk membaca <a href="https://file.io/PCQ2xQOSGTab" download>log website</a> itu dengan menggunakan script awk bernama **"soal2_forensic_dapos.sh"** yang nantinya akan ada 5 tugas yang dilakukan pada script ini.
<details>
  <summary markdown="span">Tugas 2a</summary>

    Buat folder terlebih dahulu bernama forensic_log_website_daffainfo_log. 
</details>

- Source Code

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;![2a](./img/soal2a.png)

- Cara Pengerjaan

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**Tujuan : Membuat folder bernama **forensic_log_website_daffainfo_log**.**

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Sesuai dengan screenshot source code di atas, pembuatan folder dapat dilakukan dengan syntax mkdir yang diikuti flag -p untuk membuat nested directories jika saja belum terbuat dan diikuti -- untuk mengakhiri opsi argumen kemudian nama folder yang akan dibuat dengan double quote("").

- Kendala

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Tidak ditemukan.

<details>
  <summary markdown="span">Tugas 2b</summary>

    Dikarenakan serangan yang diluncurkan ke website "https://daffa.info" sangat banyak, Dapos ingin tahu berapa rata-rata request per jam yang dikirimkan penyerang ke website.
    Kemudian masukkan jumlah rata-ratanya ke dalam sebuah file bernama ratarata.txt ke dalam folder yang sudah dibuat sebelumnya.
</details>

- Source Code

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;![2b](./img/soal2b.png)

- Cara Pengerjaan

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**Tujuan : Mengetahui rata-rata request per jam pada saat penyerangan website.**

1. Inisialisasi variabel reqs dan hours dengan nilai 0, variabel reqs untuk menghitung banyaknya request yang masuk, sedangkan variabel hours untuk menghitung lamanya penyerangan website seluruhnya.
2. Melakukan `cat` pada `log_website_daffainfo.log` untuk menyalin semua isi log yang nantinya akan digunakan untuk menemukan rata-rata request per jam.
3. Menggunakan `awk -F:` untuk memisahkan string saat terbaca `:` menjadi beberapa argumen.
4. Karena baris teratas pada log file hanyalah penanda masing-masing data yang ada, maka akan diskip 1 line dengan menggunakan `NR > 1`.
5. Menggunakan array count yang berindex `$3` atau argumen ke 3 dengan separator `:` yaitu variabel jam pada log file seperti 00, 01, 02, dan seterusnya.
6. Perhitungan dilakukan dalam pattern `END` dengan menggunakan looping/perulangan untuk index pada array count dimana pada looping akan dieksekusi penambahan request pada jam tertentu ke variabel reqs dan penambahan jam ke variabel hours sesuai dengan log file.
7. Assign nilai req per jam ke variabel avg dan akan diberikan keluaran **Rata-rata serangan adalah sebanyak** `avg` **requests per jam** dimana `avg` adalah rata-rata yang dimaksud, keluaran ini akan dimasukkan kedalam file **ratarata.txt** dengan menggunakan tanda `>` yang berarti akan membuat file jika belum ada dan jika file sudah ada, setiap keluaran baru akan menimpa keluaran yang lama.

- Kendala

1. Content log file masuk ke file **ratarata.txt**.

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Solusi : Penggunaan pipe `|` sebagai pemisah antara command `cat` dan `awk`.

2. Mendapatkan beberapa argumen untuk mendapatkan bagian jam.

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Solusi : Penggunakan opsi awk separator `-F:`.

3. Menggunakan command `echo` untuk keluaran tetapi tidak bisa.

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Solusi : Penggunaan command `printf`. 

<details>
  <summary markdown="span">Tugas 2c</summary>

    Sepertinya penyerang ini menggunakan banyak IP saat melakukan serangan ke website "https://daffa.info", Dapos ingin menampilkan IP yang paling banyak melakukan request ke server dan
    tampilkan berapa banyak request yang dikirimkan dengan IP tersebut. Masukkan outputnya kedalam file baru bernama result.txt kedalam folder yang sudah dibuat sebelumnya.
</details>

- Source Code

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;![2c](./img/soal2c.png)

- Cara Pengerjaan

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**Tujuan : Menampilkan IP yang paling banyak melakukan request ke server dan berapa banyak request yang dikirimkan dengan IP tersebut.**

1. Inisialisasi variabel res dengan nilai 0 dan variabel string kosong ipmax dimana variabel res untuk menghitung banyaknya request yang dikirim oleh IP yang paling banyak melakukan request ke server, sedangkan variabel ipmax akan memiliki value IP yang paling banyak melakukan request ke server.
2. Melakukan `cat` pada `log_website_daffainfo.log` untuk menyalin semua isi log yang nantinya akan digunakan untuk menemukan rata-rata request per jam.
3. Menggunakan `awk -F:` untuk memisahkan string saat terbaca `:` menjadi beberapa argumen.
4. Karena baris teratas pada log file hanyalah penanda masing-masing data yang ada, maka akan diskip 1 line dengan menggunakan `NR > 1`.
5. Menggunakan command `gsub(/"/,"",$1)` untuk menggantikan setiap ditemui double quote(") menjadi kosong atau dengan kata lain menghilangkan setiap double quote(") pada argumen 1 dan menggunakan array count yang berindex `$1` atau argumen ke 1 dengan separator `:` yaitu variabel IP pada log file seperti 45.146.165.37, 167.71.1.54, dan seterusnya.
6. Perhitungan dilakukan dalam pattern `END` dengan menggunakan looping/perulangan untuk index pada array count dimana pada looping akan dieksekusi pencarian max value dengan membandingkan variabel res dengan value dari count[`IP`] saat itu. Kemudian akan diperoleh max value yang tersimpan di res beserta ipmax-nya.
7. Keluaran **IP yang paling banyak mengakses server adalah:** `ipmax` **sebanyak** `res` **requests** dimana `ipmax` adalah IP yang paling banyak melakukan request ke server dan `res` adalah banyaknya request yang dikirim oleh `ipmax` tersebut. Keluaran ini akan dimasukkan kedalam file **result.txt** dengan menggunakan tanda `>` yang berarti akan membuat file jika belum ada dan jika file sudah ada, setiap keluaran baru akan menimpa keluaran yang lama.

- Kendala

1. Menghilangkan tanda petik pada bagian IP dari log file.

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Solusi : Penggunaan command `gsub(/"/,"",$1)`.

<details>
  <summary markdown="span">Tugas 2d</summary>

    Beberapa request ada yang menggunakan user-agent ada yang tidak. Dari banyaknya request, berapa banyak requests yang menggunakan user-agent curl?
    Kemudian masukkan berapa banyak requestnya kedalam file bernama result.txt yang telah dibuat sebelumnya.
</details>

- Source Code

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;![2d](./img/soal2d.png)

- Cara Pengerjaan

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**Tujuan : Menampilkan banyak requests yang menggunakan user-agent curl.**

1. Inisialisasi variabel res dengan nilai 0 dimana variabel res untuk menghitung banyak requests yang menggunakan user-agent **curl**.
2. Pada command awk menggunakan kriteria `/curl/` untuk mencari setiap line pada log file yang menggunakan user-agent **curl** kemudian menggunakan action `res+=1` untuk menambahkan hasil setiap ditemukan penggunaan **curl**.
3. Dalam pattern `END` dikeluarkan **Ada** `res` **requests yang menggunakan curl sebagai user-agent** dimana `res` adalah banyak requests yang menggunakan user-agent **curl**. Keluaran ini akan dimasukkan kedalam file **result.txt** dengan menggunakan tanda `>>` yang berarti akan menambahkan keluaran baru di bawah keluaran lama pada file yang sama dengan membaca text dari log file `log_website_daffainfo.log` pada command awk.

- Kendala

1. Mencari baris yang memiliki substring **curl**.

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Solusi : Menggunakan opsi `/curl/` pada awk sebelum action.

<details>
  <summary markdown="span">Tugas 2e</summary>

    Pada jam 2 pagi pada tanggal 23 terdapat serangan pada website, Dapos ingin mencari tahu daftar IP yang mengakses website pada jam tersebut. 
    Kemudian masukkan daftar IP tersebut kedalam file bernama result.txt yang telah dibuat sebelumnya.
</details>

- Source Code

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;![2e](./img/soal2e.png)

- Cara Pengerjaan

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**Tujuan : Mencari tahu daftar IP yang mengakses website pada jam 2 pagi.**

1. Menggunakan `awk -F:` untuk memisahkan string saat terbaca `:` menjadi beberapa argumen.
2. Pada command awk menggunakan kriteria `/2022:02:/` untuk mencari setiap line pada log file yang mengakses website pada jam 2 pagi.
3. Menggunakan command `gsub(/"/,"",$1)` untuk menggantikan setiap ditemui double quote(") menjadi kosong atau dengan kata lain menghilangkan setiap double quote(") pada argumen 1 dan menggunakan array count yang berindex `$1` atau argumen ke 1 dengan separator `:` yang memiliki substring `/2022:02:/` yaitu variabel IP pada log file seperti 128.14.133.58, 109.237.103.38, dan seterusnya.
4. Dalam pattern `END` dengan menggunakan looping/perulangan untuk index pada array count dimana pada looping akan dieksekusi memberikan keluaran index yang merupakan daftar IP yang mengakses website pada jam 2 pagi. Keluaran ini akan dimasukkan kedalam file **result.txt** dengan menggunakan tanda `>>` yang berarti akan menambahkan keluaran baru di bawah keluaran lama pada file yang sama dengan membaca text dari log file `log_website_daffainfo.log` pada command awk.

- Kendala

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Tidak ditemukan.

- Output

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;![out](./img/Output.png)

### Soal 3

Pada soal 3, kami diminta untuk membuat script **minute_log.sh** untuk mencatat informasi yang didapat dari `free -m` dan `du -sh` yang kemudian disimpan dalam file log dalam setiap menit. Kemudian juga diminta untuk membuat script **aggregate_minutes_to_hourly_log.sh** aggregasi file-file log tersebut ke dalam satuan jam, di mana terdapat nilai minimum, maximum, dan average dari data log yang tersimpan.

#### Source code minute_log.sh

![minute_log.sh](./img/1_3_1.png)

#### Cara Pengerjaan

Tujuan: Menerima hasil dari `free -m` dan `du -sh ~/log` kemudian menyimpannya dalam file log

1. Membuat directory log dengan `mkdir -p ~/log`
2. Menyimpan waktu saat ini dengan menggunakan command `date` ke dalam variabel `tdate`, `thour`, dan `tminute`
3. Mencetak tipe-tipe metric ke dalam file log menggunakan `echo`dengan redirect `>` untuk membuat file log di directory `~/log/metrics_$tdate$thour$tminute.log` sehingga file log terbentuk sesuai waktu script ini di-bash
4. Menggunakan `free -m` dan memproses hasilnya dengan `awk`, menggunakan `OFS=","` agar output dipisahkan oleh `,`, `NR!=1` karena baris pertama tidak dibutuhkan, print dimulai dari argument kedua karena argument pertama merupakan jenis data, kemudian semua hasil disimpan dalam log menggunakan redirect `>>`
5. Menggunakan `du -sh` dan juga memproses hasilnya sama seperti sebelumnya
6. Mengubah permission file log yang telah selesai menggunakan `chmod 400` yang merupakan user only read permission

#### Source code aggregate_minutes_to_hourly_log.sh

![aggregate_minutes_to_hourly_log.sh](./img/1_3_2.png)

#### Cara Pengerjaan

Tujuan: Memproses data yang telah dibentuk oleh minute_log.sh secara aggregasi kemudian menyimpannya dalam file log

1. Membuat directory log dengan `mkdir -p ~/log`
2. Menyimpan waktu saat ini ke dalam variabel `tdate`, `thour` dan juga menyimpan waktu 1 jam sebelumnya ke dalam variabel `tdatelh` dan `thourlh`
3. Menggunakan perulangan for untuk mengakses file log menit yang telah tersedia. Sebelum mengakses, cek terlebih dahulu apakah file yang dimaksud sudah ada menggunakan `test -f "$FILE"` agar tidak terjadi error apabila file tidak ditemukan. Setelah itu, menggunakan `awk` untuk mengambil data pada file log menit yang kemudian dimasukkan kedalam file temporary
4. Mencetak tipe-tipe metric ke dalam file log menggunakan `echo` dengan redirect `>` untuk membuat file log jam di directory `~/log/metrics_agg_$tdate$thour.log`
5. Menggunakan `awk` untuk memproses data yang telah dikumpulkan dalam file temporary. `-F ","` untuk memisah argument dengan `,`. Untuk nilai minimum, dalam `BEGIN`, diinialisasi 10 variabel dengan nilai yang tinggi karena nantinya akan digunakan untuk mencari nilai yang paling rendah, setiap baris (setiap data yang disimpan dalam file temporary) dibandingkan setiap argumen dengan variabel yang telah dibuat sehingga dalam `END` didapat variabel-variabel tersebut telah berisi nilai minimum untuk setiap metric dan kemudian dicetak dengan `echo` ke dalam file log jam yang telah terbentuk tadi
6. Untuk maksimum, variabel dalam `BEGIN` diinisiasi dengan nilai terendah, yakni 0 karena nantinya akan digunakan untuk mencari nilai paling tinggi
7. Sedangkan untuk average, 10 variabel digunakan untuk menyimpan jumlah dari setiap metric dari data yang ada, kemudian pada `END` setiap variabel dibagi dengan banyaknya data pada file temporary sehingga didapat nilai rata-rata. Sebelum di-print, semua variabel di-assign menjadi int terlebih dahulu dengan tujuan menghapus angka di belakang koma, sama halnya dengan fungsi `floor`
8. Menghapus file temporary yang sudah digunakan dengan command `rm`
9. Mengubah permission file log yang telah selesai menggunakan `chmod 400`

#### Contoh Crontab

![crontab](./img/1_3_3.png)

#### Penjelasan

Dalam crontab tersebut, file `minute_log.sh` dijalankan setiap menit dan file `aggregate_minutes_to_hourly_log.sh` dijalankan setiap jam

#### Kendala

Tidak ditemukan

#### Contoh Output minute_log.sh

![output minute_log.sh](./img/1_3_4.png)

#### Contoh Output aggregate_minutes_to_hourly_log.sh

![output aggregate_minutes_to_hourly_log.sh](./img/1_3_5.png)

## Referensi Soal Shift 1

1. https://www.guru99.com/linux-pipe-grep.html#:~:text=is%20a%20Filter%3F-,What%20is%20a%20Pipe%20in%20Linux%3F,'%7C'%20denotes%20a%20pipe.
2. https://www.gnu.org/software/gawk/manual/html_node/String-Functions.html
3. https://stackoverflow.com/questions/2609552/how-can-i-use-as-an-awk-field-separator
4. https://linux.vpslabs.net/contoh-perintah-awk-di-linux/
5. https://stackoverflow.com/questions/29340587/comparison-of-cat-pipe-awk-operation-to-awk-command-on-a-file
6. https://linuxhint.com/skip_line_awk/
7. https://stackoverflow.com/questions/27060531/how-to-count-requests-per-hour

