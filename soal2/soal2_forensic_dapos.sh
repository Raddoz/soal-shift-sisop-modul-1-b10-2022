#!/bin/bash

#2a
mkdir -p -- "forensic_log_website_daffainfo_log"

#2b
reqs=0
hours=0
cat "log_website_daffainfo.log" |
awk -F: '
NR > 1 { count[$3]++ };
    END {
        for(i in count)
        {
            reqs+=count[i]
            hours+=1
        }
        avg=reqs/(hours-1)
        printf "Rata-rata serangan adalah sebanyak %.1f requests per jam\n", avg
    }
' > "forensic_log_website_daffainfo_log/ratarata.txt"

#2c
res=0
ipmax=""
cat "log_website_daffainfo.log" |
awk -F: '
NR > 1 { gsub(/"/, "", $1); count[$1]++ }
    END {
        for(i in count)
            if(count[i] > res)
            {
                res=count[i]
                ipmax = i
            }
        printf "IP yang paling banyak mengakses server adalah: %s sebanyak %d requests\n\n", ipmax, res
    }
' > "forensic_log_website_daffainfo_log/result.txt"

#2d
res=0
awk '/curl/ { res+=1 }
    END{
       printf "Ada %d requests yang menggunakan curl sebagai user-agent\n\n", res
    }
' log_website_daffainfo.log >> forensic_log_website_daffainfo_log/result.txt

#2e
awk -F: '
/2022:02:/ { gsub(/"/, "", $1); count[$1]++ }
    END{
        for(i in count)
            print i
    }
' log_website_daffainfo.log >> forensic_log_website_daffainfo_log/result.txt
