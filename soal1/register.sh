#!/bin/bash

Username(){
read -p "Username: " username

if [ ! -d "users" ]
then
    mkdir users
    touch users/user.txt
fi

if grep -q -w $username ./users/user.txt
then
    echo "Username sudah dipakai"
    printf '%(%m/%d/%y %H:%M:%S)T REGISTER: ERROR User already exists\n' >> log.txt
    exit 0
fi
}

Password (){
read -s -p "Password: " password
echo ""  
 if [ ${#password} -lt 8 ]
  then
    echo -e "\nPassword minimal 8 karakter!"
    exit 0
  elif [[ $password = ${password,,} || $password = ${password^^} ]]
  then
    echo -e "\nPassword memiliki minimal 1 huruf kapital dan 1 huruf kecil!"
    exit 0
  elif [[ "$password" =~ [^a-zA-Z0-9] ]]
  then 
    echo -e "\nPassword alphanumeric!"
    exit 0
  fi
  echo $password | grep -q [0-9]
  if test $? -ne 0
  then
    echo -e "\nPassword alphanumeric!"
    exit 0
  elif [[ "$password" = "$username" ]]
  then 
    echo -e "\nPassword tidak boleh sama dengan username!"
    exit 0
  fi
}

echo "Register Your User"

Username
Password

echo "$username $password" >> ./users/user.txt
	printf '%(%m/%d/%y %H:%M:%S)T REGISTER:INFO '$username' registered succesfully\n' >> log.txt
