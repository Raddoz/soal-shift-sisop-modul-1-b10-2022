#!/bin/bash

Login(){
    read -p "Username: " username
    read -s -p "Password: " password
    echo ""
    if grep -q -w "$username $password" ./users/user.txt
    then
        echo "Logged in"
        printf '%(%m/%d/%y %H:%M:%S)T LOGIN: INFO User '$username' logged in\n' >> log.txt
    else
        echo "Not logged in"
        printf '%(%m/%d/%y %H:%M:%S)T LOGIN: ERROR Failed login attempt on user '$username'\n' >> log.txt
        exit 0
    fi
}

Command () {
  echo "Input Command: "
  read command n

  if [[ $command = "dl" ]]
  then
    doZip
    dl_files
  elif [[ $command = "att" ]]
  then
    awk -v temp=$username '/LOGIN/&&$0~" " temp " " {++n; print} END {print "Login Attempt: " n}' log.txt
  fi
}

doZip () {
  folder_name=`printf '%(%Y-%m-%d)T'`_$username
  if ! [ -f "$folder_name.zip" ]
  then
    mkdir $folder_name
  else
    unzip -e $folder_name.zip
    count=`ls ./$folder_name | wc -l`
    n=$((n + count))
  fi
}


dl_files () {
  for (( i=$count+1; i <= n; i++ ));
  do
    wget https://loremflickr.com/320/240 -O $folder_name/PIC_$i
  done
  zip -r -e $folder_name $folder_name
  rm -r $folder_name
}


Login
Command



